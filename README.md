# Tamarin Models

This repo contains the formal models for verifying the L1RP protocol described
in the paper [Practical EMV Relay Protection](practical_emv.gitlab.io), Section
VII, as well as the models for the mobile versions of Visa/Mastercard.


